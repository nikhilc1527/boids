#include <SDL2/SDL.h>

#include <iostream>
#include <thread>
#include <fstream>
#include <vector>
#include <numeric>
#include <numbers>
#include <functional>
#include <iomanip>
#include <sstream>

#include <glm/glm.hpp>
#include <fmt/format.h>

#include <unistd.h>
#include <fcntl.h>

using namespace std::chrono_literals;

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define VISION_RAD 50
#define NUM_BOIDS 50
#define FPS 60
#define SPEED_LIMIT 35.0f
#define COHESION_FACTOR   0.5f // steer towards average position of neighbors
#define ALIGNMENT_FACTOR  0.5f // steer towards average velocity of neighbors
#define SEPARATION_FACTOR 300.0f // steer away from closeby neighbors
#define WALL_REPEL_FACTOR 300.0f
#define SPEED_CONSTANT 120.0f
#define BOOSTER 1.01f
// #define WALL_BOUNCE
#define WALL_REPEL
#define TIME_LIMIT 24h

float my_lerp(float x, float y, float t) {
  return (x - y) * t + y;
}

double get_rand() {
  static std::ifstream rand_stream("/dev/urandom");
  int precision = 2;
  double rand = 0;
  for (int i = 0; i < precision; ++i) {
    rand = rand * 256 + rand_stream.get();
  }

  for (int i = 0; i < precision; ++i) {
    rand /= 256.0;
  }

  return rand;
}

int ind(int x, int y) { return x + y * WINDOW_WIDTH; }

std::uint32_t rgb(std::uint8_t r, std::uint8_t g, std::uint8_t b) {
  return (b << 8 * 3) | (g << 8 * 2) | (r << 8 * 1);
}

std::uint32_t HSBtoRGB(int hue, int sat, int bright) {
  int colors[3];

  float max_rgb_val = 255;

  float sat_f = float(sat) / 100.0;
  float bright_f = float(bright) / 100.0;
  std::uint8_t r, g, b;

  if (bright <= 0) {
    colors[0] = 0;
    colors[1] = 0;
    colors[2] = 0;
  }

  if (sat <= 0) {
    colors[0] = bright_f * max_rgb_val;
    colors[1] = bright_f * max_rgb_val;
    colors[2] = bright_f * max_rgb_val;
  }

  else {
    if (hue >= 0 && hue < 120) {
      float hue_primary = 1.0 - (float(hue) / 120.0);
      float hue_secondary = float(hue) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
      g = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
      b = (bright_f * max_rgb_val) * sat_tertiary;
    }

    else if (hue >= 120 && hue < 240) {
      float hue_primary = 1.0 - ((float(hue) - 120.0) / 120.0);
      float hue_secondary = (float(hue) - 120.0) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * sat_tertiary;
      g = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
      b = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
    }

    else // if (hue >= 240 && hue <= 360)
      {
      float hue_primary = 1.0 - ((float(hue) - 240.0) / 120.0);
      float hue_secondary = (float(hue) - 240.0) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
      g = (bright_f * max_rgb_val) * sat_tertiary;
      b = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
    }

    colors[0] = r;
    colors[1] = g;
    colors[2] = b;
  }

  r = colors[0];
  g = colors[1];
  b = colors[2];

  return rgb(r, g, b);
}

void draw_line(std::uint32_t *pixels, std::uint32_t color, int x1, int y1, int x2, int y2) {
  float steps = 50.0f;
  for (int i = 0; i <= steps; ++i) {
    // c++ 20 std::lerp (https://en.cppreference.com/w/cpp/numeric/lerp)
    // ccls doesnt like c++20 stuff yet so im reimplementing lerp
    auto x = my_lerp(x1, x2, i / steps);
    auto y = my_lerp(y1, y2, i / steps);
    if (x < 0 || x >= WINDOW_WIDTH || y < 0 || y >= WINDOW_HEIGHT) continue;
    int idx = ind(x, y);
    if (idx < 0 || idx >= WINDOW_WIDTH * WINDOW_HEIGHT) continue;
    pixels[idx] = color;
  }
}

void draw_triangle(std::uint32_t *pixels, std::uint32_t color, int x1, int y1, int x2, int y2, int x3, int y3) {
  // climb up two of the sides of the triangle and keep drawing lines between those two sides
  float steps = 200.0f;
  for (int i = 0; i <= steps; ++i) {
    auto x4 = my_lerp(x1, x3, i / steps);
    auto y4 = my_lerp(y1, y3, i / steps);
    auto x5 = my_lerp(x2, x3, i / steps);
    auto y5 = my_lerp(y2, y3, i / steps);
    if (x4 < 0 || x4 >= WINDOW_WIDTH || y4 < 0 || y4 >= WINDOW_HEIGHT) continue;
    if (x5 < 0 || x5 >= WINDOW_WIDTH || y5 < 0 || y5 >= WINDOW_HEIGHT) continue;
    draw_line(pixels, color, x4, y4, x5, y5);
  }
}

std::string byte_to_hexadecimal(std::uint8_t b) {
  std::string res;
  if (b / 16 >= 10) res.push_back('a' + b / 16 - 10);
  else res.push_back('0' + b / 16);
  if (b % 16 >= 10) res.push_back('a' + b % 16 - 10);
  else res.push_back('0' + b % 16);
  return res;
}

struct Boid {
  const static int seed_size = 2;
  std::uint8_t x_seed[seed_size];
  std::uint8_t y_seed[seed_size];
  glm::vec2 pos;
  std::uint8_t vx_seed[seed_size];
  std::uint8_t vy_seed[seed_size];
  glm::vec2 vel;
  glm::vec2 acc;

  bool predator;

  Boid() : pos(0, 0), vel(0, 0), acc(0, 0), predator(false) {
    static std::ifstream rand_stream("/dev/urandom");
    for (int i = 0; i < seed_size; ++i) {
      x_seed[i] = rand_stream.get();
      pos.x *= 256.0;
      pos.x += x_seed[i];
      
      y_seed[i] = rand_stream.get();
      pos.y *= 256.0;
      pos.y += y_seed[i];
      
      vx_seed[i] = rand_stream.get();
      vel.x *= 256.0;
      vel.x += vx_seed[i];
      
      vy_seed[i] = rand_stream.get();
      vel.y *= 256.0;
      vel.y += vy_seed[i];
    }
    for (int i = 0; i < seed_size; ++i) {
      pos /= 256.0f;
      vel /= 256.0f;
    }
    pos.x *= WINDOW_WIDTH;
    pos.y *= WINDOW_HEIGHT;
    vel.x = vel.x * 2 - 1;
    vel.y = vel.y * 2 - 1;
    vel *= SPEED_LIMIT;
  }

  Boid(std::uint8_t *bytes) : pos(0, 0), vel(0, 0), acc(0, 0), predator(false) {
    for (int i = 0; i < seed_size; ++i) {
      x_seed[i] = bytes[seed_size * 0 + i];
      pos.x *= 256.0;
      pos.x += x_seed[i];
      
      y_seed[i] = bytes[seed_size * 1 + i];
      pos.y *= 256.0;
      pos.y += y_seed[i];

      vx_seed[i] = bytes[seed_size * 2 + i];
      vel.x *= 256.0;
      vel.x += vx_seed[i];
      
      vy_seed[i] = bytes[seed_size * 3 + i];
      vel.y *= 256.0;
      vel.y += vy_seed[i];
    }
    for (int i = 0; i < seed_size; ++i) {
      pos /= 256.0f;
      vel /= 256.0f;
    }
    pos.x *= WINDOW_WIDTH;
    pos.y *= WINDOW_HEIGHT;
    vel.x = vel.x * 2 - 1;
    vel.y = vel.y * 2 - 1;
    vel *= SPEED_LIMIT;
  }

  void dump_seed(std::ostream &output) const {
    for (int i = 0; i < seed_size; ++i) {
      output << x_seed[i];
    }
    for (int i = 0; i < seed_size; ++i) {
      output << y_seed[i];
    }
    for (int i = 0; i < seed_size; ++i) {
      output << vx_seed[i];
    }
    for (int i = 0; i < seed_size; ++i) {
      output << vy_seed[i];
    }
  }
};

void update_boids(std::vector<Boid> &boids, float dt) {
  for (Boid &b : boids) {
    glm::vec2 avg_pos(0, 0);
    glm::vec2 repel(0, 0);
    glm::vec2 avg_vel(0, 0);
    int num = 0;
    for (const Boid &b2 : boids) {
      if (&b == &b2) continue;
      auto dist = glm::distance(b.pos, b2.pos);
      if (!b.predator && (dist > VISION_RAD || dist < 1.0f))
        continue;
      if (b.predator && (dist > VISION_RAD * 3 || dist < 1.0f))
        continue;
      num++;
      
      if (!b2.predator) avg_pos += b2.pos - b.pos;
      else avg_pos -= (b2.pos - b.pos) * 10.0f;
      if (!b2.predator) avg_vel += b2.vel - b.vel;
      else avg_vel += (b2.vel - b.vel) * 10.0f;
      repel += (b.pos - b2.pos) / dist / dist;
      if (b.predator) repel /= 30.0f;
      if (b2.predator) repel *= 30.0f;
    }

    if (num) {
      avg_pos /= num;
      avg_vel /= num;

      avg_pos *= COHESION_FACTOR;
      avg_vel *= ALIGNMENT_FACTOR;
      if (b.predator) avg_vel *= 0.01f;
      repel *= SEPARATION_FACTOR;

      b.acc = avg_pos + repel + avg_vel;
    }

    glm::vec2 wall_repel(0);

#ifdef WALL_REPEL
    const float wall_repel_min = 0.01f;

    wall_repel.x += 1.0f / std::max( wall_repel_min, b.pos.x);
    wall_repel.x += 1.0f / std::min(-wall_repel_min, b.pos.x - WINDOW_WIDTH);
    wall_repel.y += 1.0f / std::max( wall_repel_min, b.pos.y);
    wall_repel.y += 1.0f / std::min(-wall_repel_min, b.pos.y - WINDOW_HEIGHT);
    
    wall_repel *= WALL_REPEL_FACTOR;

    b.acc += wall_repel;
#endif
  }
  
  for (Boid &b : boids) {
    b.vel = b.vel + (b.acc * dt);
    b.vel *= BOOSTER; // a little boost if everybody starts going very slowly
    if (!b.predator) {
      if (glm::length(b.vel) > SPEED_LIMIT)
        b.vel = glm::normalize(b.vel) * SPEED_LIMIT;
    }
    if (b.predator) {
      if (glm::length(b.vel) > SPEED_LIMIT * 1.05f)
        b.vel = glm::normalize(b.vel) * SPEED_LIMIT * 1.05f;
    }
    b.pos = b.pos + (b.vel * dt);
    b.acc = glm::vec2(0, 0);

#ifdef WALL_BOUNCE
    // dont let them go out of bounds
    if (b.pos.x < 0 || b.pos.x >= WINDOW_WIDTH) {
      b.pos.x -= b.vel.x;
      // b.vel.x *= -1;
    }
    if (b.pos.y < 0 || b.pos.y >= WINDOW_HEIGHT) {
      b.pos.y -= b.vel.y;
      // b.vel.y *= -1;
    }
#else
    // position wrapping
    if (b.pos.x < 0)
      b.pos.x += WINDOW_WIDTH;
    else if (b.pos.x >= WINDOW_WIDTH)
      b.pos.x -= WINDOW_WIDTH;
    if (b.pos.y < 0)
      b.pos.y += WINDOW_HEIGHT;
    else if (b.pos.y >= WINDOW_HEIGHT)
      b.pos.y -= WINDOW_HEIGHT;
#endif
  }
}

void draw_boids(std::vector<Boid> &boids, SDL_Texture *texture) {
    void *pixels_;
    int pitch;
    SDL_LockTexture(texture, nullptr, &pixels_, &pitch);
    std::uint32_t *pixels = (std::uint32_t*)pixels_;

    for (int i = 0; i < WINDOW_WIDTH * WINDOW_HEIGHT; ++i) {
      pixels[i] = rgb(0, 0, 0);
    }

    for (const auto &b : boids) {
      glm::vec2 rotate_90 = glm::normalize(b.vel) * glm::mat2x2(std::cos(std::numbers::pi/2), -std::sin(std::numbers::pi/2), std::sin(std::numbers::pi/2), std::cos(std::numbers::pi/2)) * 5.0f;
      std::uint32_t color;
      if (!b.predator) {
        // auto lerped = my_lerp(0, 255, glm::length(b.vel) / SPEED_LIMIT);
        // color = rgb(255 - lerped, lerped, 0);
        color = HSBtoRGB((std::atan2(b.vel.y, b.vel.x) + std::numbers::pi) / std::numbers::pi * 180, glm::length(b.vel) / SPEED_LIMIT * 100, 100);
      }
      if (b.predator) color = rgb(0, 0, 0);
      draw_triangle(pixels, color, (b.pos + rotate_90).x, (b.pos + rotate_90).y, (b.pos - rotate_90).x, (b.pos - rotate_90).y, (b.pos + b.vel).x, (b.pos + b.vel).y);
    }

    SDL_UnlockTexture(texture);
}

void dump_boids(std::vector<Boid> *boids) {
  std::ofstream output_file("dump.boids");
  fmt::print("[INFO] dumping boids.boids");
  for (const Boid &b : *boids) {
    b.dump_seed(output_file);
  }
}

void event_listener(bool *quit, bool *pause, bool *reset, std::vector<Boid> *boids) {
  auto beginning = std::chrono::high_resolution_clock::now();
  while (!(*quit)) {
    SDL_Event event;
    SDL_PollEvent(&event);
    if (event.type == SDL_QUIT) {
      std::cout << "hehe i dont care"
                << "\n";
    }
    if (event.type == SDL_KEYDOWN) {
      if (event.key.keysym.scancode == SDL_SCANCODE_Q) {
        *quit = true;
      }
      if (event.key.keysym.scancode == SDL_SCANCODE_SPACE) {
        *pause = !(*pause);
      }
      if (event.key.keysym.scancode == SDL_SCANCODE_G) {
        *reset = true;
      }
      if (event.key.keysym.scancode == SDL_SCANCODE_R) {
        dump_boids(boids);
      }
    }

    if (std::chrono::high_resolution_clock::now() - beginning > TIME_LIMIT) {
      *quit = true;
    }

    std::this_thread::sleep_for(10ms);
  }
}

void gen_boids(std::vector<Boid> &boids) {
  boids.clear();
  
  for (int i = 0; i < NUM_BOIDS; ++i) {
    boids.emplace_back();
  }
}

void gen_boids_from_file(std::vector<Boid> &boids, const std::string &filepath) {
  int fd = open(filepath.c_str(), O_RDONLY);
  boids.clear();
  const int buf_size = Boid::seed_size * 4;
  std::uint8_t buffer[buf_size];
  for (int i = 0; i < NUM_BOIDS; ++i) {
    read(fd, buffer, buf_size);
    boids.emplace_back(buffer);
  }
  close(fd);
}

int main(int argc, char *argv[]) {
  (void) argc;
  (void) argv;

  fmt::print("my pid: {}\n", getpid());
  
  SDL_Init(SDL_INIT_VIDEO);

  SDL_Window *window =
      SDL_CreateWindow("Boids", 10, 10, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB32,
                                           SDL_TEXTUREACCESS_STREAMING,
                                           WINDOW_WIDTH, WINDOW_HEIGHT);

  std::vector<Boid> boids;
  // gen_boids_from_file(boids, "dump.boids");
  gen_boids(boids);

  bool quit = false;
  bool pause = false;
  bool reset = false;

  std::thread event_listener_t(event_listener, &quit, &pause, &reset, &boids);
  auto now = std::chrono::high_resolution_clock::now();
  auto dt = now - now;
  while (!quit) {
    if (reset) {
      gen_boids(boids);
      reset = false;
    }
    
    if (!pause)
      update_boids(boids, dt.count() / 1000000.0 / SPEED_CONSTANT);

    draw_boids(boids, texture);

    SDL_RenderCopy(renderer, texture, nullptr, nullptr);
    SDL_RenderPresent(renderer);
    
    auto now2 = std::chrono::high_resolution_clock::now();
    dt = now2 - now;

    auto sleep = (1000ms / (double)(FPS)) - dt;
    if (sleep < 0s) sleep = 0s;
    std::this_thread::sleep_for(sleep);

    now2 = std::chrono::high_resolution_clock::now();
    dt = now2 - now;    
    now = now2;
  }

  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  event_listener_t.join();
  
  return 0;
}
