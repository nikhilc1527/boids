CXXFLAGS = -O3 -std=c++20 -Wall -Wextra -pedantic
LDFLAGS = -lSDL2 -lfmt -pthread
OUTPUT = main

main: main.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) main.cpp -o $(OUTPUT)
