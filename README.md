# Boids (in SDL2)

This is my implementation of boids. Boids is a simulation of how birds flock in the wild. My implementation is a 2D version made using the [SDL2](https://www.libsdl.org/download-2.0.php) multimedia (graphics) library. The FPS can be limited with the FPS macro at the top, in order to prevent overuse of the cpu. 

## Boids
The 3 main behaviors that they show are:  
**collision avoidance(separation)**: trying not to run into any other  
**velocity matching(alignment)**: trying to match the velocity (speed and direction) of the nearby boids  
**flock centering(cohesion)**: trying to move towards the center of their current group of nearby boids  
as originally detailed by Craig Reynolds (you can read a copy of the original paper [here](https://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/)). These three behaviors together make the effect of a group of birds flying together in a flock. The three factors of cohesion, separation, and alignment can also be controlled using the corresponding macros at the top. 

## Predators
There are also predators, which do not care about alignment or separation, but mostly cohesion, since they want to be as close to the boids as possible. On the other hand, the boids have extra separation from predators

## Examples

![](https://gitlab.com/nikhilc1527/boids/-/raw/master/gif.gif)
![](https://gitlab.com/nikhilc1527/boids/-/raw/master/gif2.gif)
![](https://gitlab.com/nikhilc1527/boids/-/raw/master/gif3.gif)
![](https://gitlab.com/nikhilc1527/boids/-/raw/master/gif4.gif)
![](https://gitlab.com/nikhilc1527/boids/-/raw/master/gif5.gif)
